 MuMag Standard Problem #1, FIELD ALONG X AXIS     !comment line
 1100.      20.        2100.      !simulation size (nm)
 110        2          210        !# of simulation grids
 0          0                     !# of grid layers of substrate and film (if nf=0, the system is considered either a bulk or an island) (ns only for nf/=0 or ni3/=0)
 1                                !island in-plane shape: 0-2d array in 'islandShape.in' 1-rectangular or 2-elliptical shape defined in the next line (only for nf=0 and ni3/=0)
 100        2          200        !# of grids in the magnetic island (only for nf=0; if nf=0 and ni3=0, the system is considered bulk; ni1 and ni2 only for ni3≠0 and choiceIslandShape=1,2)
 1                                !grain structure: 0-Euler angles phi, theta and psi (degree) array in 'eulerAng.in' 1-single crystal with specified Euler angles
 0.         0.         0.         !Euler angles phi, theta and psi (degree) of the single crystal orientation (only for choiceGrainStruct=1)

 800.E3     2.211E5    0.5        !saturation magnetization (A/m), electron gyromagnetic ratio (m/(A.s)), & damping constant (unitless)

 true                             !whether to consider magnetocrystalline anisotropy
 2                                !type of magnetocrystalline anisotropy: 1-cubic 2-uniaxial (only for flagAnisotropy=true)
 5.E2       0.         0.         !magnetocrystalline anisotropy coefficient (J/m^3) (only for flagAnisotropy=true; kc3 only for choiceAnisotropy=1)

 true                             !whether to consider stray field
 1                                !type of magnetostatic boundary condition: 1-finite-size 2-periodic 3-IP finite-size, OOP periodic
 0.         0.         0.         !demagnetizing factor N11, N22, N33 (unitless) (only for flagStrayField=true and flagPeriodic=true)
 0.         0.         0.         !demagnetizing factor N23, N13, N12 (unitless) (only for flagStrayField=true and flagPeriodic=true)

 0                                !external magnetic field: 0-array in 'hExt.in' with linear interpolations 1-DC & AC components defined in following three lines
 0.         0.         0.         !DC component of external magnetic field (A/m) (only for choiceHExt=1)
 0.         0.         0.         !peak amplitude of AC component of external magnetic field (A/m) (only for choiceHExt=1)
 0.                               !frequency of AC component of external magnetic field (Hz) (only for choiceHExt=1)

 false                            !whether to consider magnetoelastic effect
 0.         0.                    !saturation magnetostriction (unitless) (only for flagElastic=true)
 0.         0.         0.         !elastic stiffness (Voigt) (Pa) (only for flagElastic=true)
 0.         0.         0.         !elastic stiffness of substrate (Voigt) (Pa) (only for flagElastic=true)
 1                                !type of bulk elastic boundary condition: 1-strain 2-stress (only for bulk, i.e. nf=0 and ni3=0, and flagElastic=true)
 0                                !applied external strain/stress: 0-array in 'sExt.in' with linear interpolations 1-static stress/strain defined in following two lines
 0.         0.         0.         !in-plane substrate/applied strains/stress (1 or Pa) (only for flagElastic=true)
 0.         0.         0.         !out-of-plane applied strains/stress (1 or Pa) (only for bulk, i.e. nf=0 and ni3=0, and flagElastic=true)
 0          0.                    !recursion limit & tolerance of convergence for iterative elastic solver (only when the elastic stiffness of the system is inhomogeneous)
 false                            !whether to input the solved strain distribution of the initial evolution step from 'strain.in' (only when the elastic stiffness of the system is inhomogeneous)

 1.3E-11                          !magnetic exchange constant (J/m)

 false                            !whether to consider Dzyaloshinskii-Moriya interaction (DMI)
 0.                               !continuous effective DMI constant (J/m^2) (only for flagDMI=true)

 false                            !whether to consider spin torque
 1                                !type of spin torque: 1-spin-orbit torque 2-Slonczewski spin-transfer torque 3-Zhang-Li spin-transfer torque (only for flagST=true)
 0.                               !spin Hall angle (only for flagST=true and choiceST=1)
 0.                               !spin-polarized electric current density in spin-orbit torque or Slonczewski spin-transfer torque(A/m^2) (only for flagST=true and choiceST=1,2)
 0.         0.         0.         !magnetization in the fixed layer in a spin torque structure (unitless) (only for flagST=true and choiceST=1,2)
 0.                               !spin polarization constant (only for flagST=true and choiceST=2)
 0.                               !degree of non-adiabaticity (only for flagST=true and choiceST=3)
 0.         0.         0.         !spin-polarized electric current density in Zhang-Li spin-transfer torque (A/m^2) (only for flagST=true and choiceST=3)

 false                            !whether to consider thermal fluctuation
 0.                               !temperature (K) (only for flagThermalFluc=true)

 1                                !type of LLGSolver: 1-implicit Gauss-Seidel using Fourier spectral 2-explicit rk4
 1.E-13                           !time per evolution step (s)
 0          1250000               !starting time step #; finishing time step #
 500        50000                 !output step interval for data table; output step interval for spatial distribution
 1                                !initial m distribution: 0-'magnt.in' 1-random orientation 2-specified uniform orientation 3-specified vortex domain
 0.         0.         0.         !axis of a uniform orientation, or a vortex domain, for the initial m distribution (only for choiceInitialM=2,3)

 false      false                 !whether to output distribution of effective fields and energy densities
 false                            !whether to output distribution of eigenstrain, strain, and stress (only for flagElastic=true)
